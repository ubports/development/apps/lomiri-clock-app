Source: lomiri-clock-app
Priority: optional
Maintainer: UBports Developers <devs@ubports.com>
Build-Depends: cmake,
               pkgconf,
               debhelper-compat (= 12),
               dh-migrations | hello,
               gettext,
               intltool,
               python3 | python3-all | python3-dev | python3-all-dev,
               python3-minimal,
               libgeonames-dev,
               lomiri-sounds,
               suru-icon-theme | ubuntu-mobile-icons,
               qml-module-qttest,
               qml-module-qtsysteminfo,
               qml-module-qt-labs-settings,
               qml-module-u1db,
               qml-module-qtmultimedia,
               qml-module-qtpositioning,
               qml-module-lomiri-content,
               qtbase5-dev,
               qtdeclarative5-dev,
               qtdeclarative5-dev-tools,
               qml-module-qt-labs-folderlistmodel,
               qml-module-lomiri-components,
               qml-module-lomiri-layouts,
               qml-module-lomiri-test,
               xauth,
               xvfb,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Section: misc
Homepage: https://gitlab.com/ubports/development/apps/lomiri-clock-app
Vcs-Git: https://gitlab.com/ubports/development/apps/lomiri-clock-app.git
Vcs-Browser: https://gitlab.com/ubports/development/apps/lomiri-clock-app
XS-Testsuite: autopkgtest

Package: lomiri-clock-app
Architecture: any
Depends: python3:any,
         qml-module-u1db,
         qml-module-lomiri-components,
         qml-module-lomiri-layouts,
         qml-module-qtquick-xmllistmodel,
         qml-module-qtmultimedia,
         qml-module-qtpositioning,
         qml-module-lomiri-content,
         lomiri-sounds,
         suru-icon-theme | ubuntu-mobile-icons,
         ${misc:Depends},
         ${shlibs:Depends},
Description: Clock App for Lomiri Operating Environment
 This app is a core app for Ubuntu Touch's shell Lomiri. Ubuntu Touch is
 a mobile OS developed by the UBports Foundation. Lomiri is its operating
 environment optimized for touch based human-machine interaction, but
 also supports convergence (i.e. switching between tablet/phone and
 desktop mode).
 .
 This package provides Lomiri's Clock App which provides Alarm, Clock and
 World Clock functionalities.

Package: lomiri-clock-app-autopilot
Architecture: all
Depends: libautopilot-qt,
         libqt5test5,
         python3-lxml,
         lomiri-clock-app (>= ${source:Version}),
         lomiri-ui-toolkit-autopilot,
         ${misc:Depends},
         ${python3:Depends},
Description: Clock App for Lomiri Operating Environment (Autopilot tests)
 This app is a core app for Ubuntu Touch's shell Lomiri. Ubuntu Touch is
 a mobile OS developed by the UBports Foundation. Lomiri is its operating
 environment optimized for touch based human-machine interaction, but
 also supports convergence (i.e. switching between tablet/phone and
 desktop mode).
 .
 This package contains autopilot tests for Lomiri Clock App.
